# Đồ án môn học
## Nhóm 9 - Phát triển ứng dụng web 
- Trường Đại học Khoa học tự nhiên - Đại học Quốc Gia Hà Nội.
- Khoa: Toán - Cơ - Tin học.

## Người hướng dẫn
- Thầy: Ngô Quốc Hùng
- Cô: Trần Thị Huyền Trang

## Thành viên nhóm
1. Nguyễn Phương Lan - 19000442
2. Nguyễn Sỹ Phi Long - 19000445
3. Nguyễn Đức Lộc - 19000446
4. Đỗ Xuân Mạnh - 19000448
5. Đặng Đức Minh - 19000450
6. Nguyễn Hoài Nam - 19000452
7. Đinh Hà Phương - 19000464
8. Trần Hữu Sơn - 19000474
9. Hoàng Đức Trung - 19000497
10. Nguyễn Quang Trường - 19000498


## Đề tài: Website quản lý thư viện
### Thiết kế hệ thống
Mockup design: https://e8u72q.axshare.com/#id=5w2365&p=login&g=1


## Setup:
### IDE:
#### PhpStorm :
Download : https://www.jetbrains.com/phpstorm/download/#section=windows

#### Visual Studio Code :
Download : https://code.visualstudio.com/Download
* khuyến khích cài extendsion PHP để code (có thể validate syntax , map các hàm , genarate code , ...)
* ![Untitled](/uploads/1379020f731461ab75de5dfbe9229d25/Untitled.png)

### Database:
#### Data setup public : 
* host : 52.53.203.35
* port : 3306
* admin : root
* password : minhdang

#### Nếu đã sử dụng docker , hãy start docker và chạy câu lệnh : 
*docker run --name mysql  -dp 3306:3306 -e MYSQL_ROOT_PASSWORD=root  mysql:5.7-oracle  để tải và chạy image Mysql docker với cấu hình :
* host : localhost
* port : 3306
* admin : root
* password : root

#### Hoặc có thể cài đặt XAMPP hoặc MySQL server : 
* MySQL sever : https://dev.mysql.com/downloads/mysql/
* XAMPP : https://www.apachefriends.org/download.html

#### Sau đó chạy dll trong folder ddl để tạo database và tạo bảng :
![data](/uploads/3164964a86d105f8dea64ee9ed3446ce/data.png)


## Sản phẩm

