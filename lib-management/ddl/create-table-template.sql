-- Active: 1672474285986@@localhost@3306@Lib


CREATE DATABASE gr09_dev
    DEFAULT CHARACTER SET = 'utf8mb4';
    
CREATE TABLE admin(  
    id INT(10) PRIMARY KEY AUTO_INCREMENT,
    login_id VARCHAR(20),
    password VARCHAR(64),
    active_flag int(1),
    reset_password_token VARCHAR(255),
    updated DATETIME,
    created DATETIME
);  

ALTER TABLE admin
ADD UNIQUE (login_id);

CREATE TABLE user(  
    id INT(10) PRIMARY KEY AUTO_INCREMENT,
    type INT(1) DEFAULT 1,
    name VARCHAR(250),
    login_id VARCHAR(20),
    avatar VARCHAR(250),
    description TEXT,
    updated DATETIME,
    created DATETIME
);

ALTER TABLE user
ADD UNIQUE (login_id);


CREATE TABLE book(  
    id INT(10) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(250),
    category VARCHAR(10),
    author VARCHAR(250),
    quantity int(3),
    avatar VARCHAR(250),
    description TEXT,
    updated DATETIME,
    created DATETIME
);


  CREATE TABLE books_transactions (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    book_id int(10),
    user_id int(10),
    borrowed_date date,
    return_plan_date date,
    return_actual_date date,
    description text,
    updated datetime,
    created datetime
  );



