INSERT INTO gr09_dev.admin (login_id, password, active_flag, reset_password_token, updated, created) VALUES ('a', '12345678', 1, '', null, '2023-01-19 00:43:21');
INSERT INTO gr09_dev.admin (login_id, password, active_flag, reset_password_token, updated, created) VALUES ('b', '12345678', 1, '', null, '2023-01-20 00:43:21');
INSERT INTO gr09_dev.admin (login_id, password, active_flag, reset_password_token, updated, created) VALUES ('c', '12345678', 1, '', null, '2023-01-21 00:43:21');
INSERT INTO gr09_dev.admin (login_id, password, active_flag, reset_password_token, updated, created) VALUES ('d', '12345678', 1, '', null, '2023-01-22 00:43:21');

INSERT INTO gr09_dev.book (name, category, author, quantity, avatar, description, updated, created) VALUES ('Dế mèn phiêu lưu ký', 'Hư cấu', 'Tô Hoài', 10, './web/image/book/test.jpg', null, null, '2023-01-19 00:45:33');

INSERT INTO gr09_dev.book (name, category, author, quantity, avatar, description, updated, created) VALUES ('Hồng Lâu Mộng', 'Tiểu thuyết', 'Tào Tuyết Cần', 11, './web/image/book/test.jpg', null, null, '2023-01-20 00:45:33');

INSERT INTO gr09_dev.book (name, category, author, quantity, avatar, description, updated, created) VALUES ('Truyện Kiều', 'Truyện thơ nôm', 'Nguyễn Trãi', 11, './web/image/book/test.jpg', null, null, '2023-01-21 00:45:33');

INSERT INTO gr09_dev.book (name, category, author, quantity, avatar, description, updated, created) VALUES ('Tắt đèn', 'Tiểu thuyết', 'Ngô Tất Tố', 10, './web/image/book/test.jpg', null, null, '2023-01-22 00:45:33');

INSERT INTO gr09_dev.book (name, category, author, quantity, avatar, description, updated, created) VALUES ('Nhật ký trong tù', 'Nhật Ký', 'Hồ Chí Minh', 10, './web/image/book/test.jpg', null, null, '2023-01-23 00:45:33');

INSERT INTO gr09_dev.books_transactions (book_id, user_id, borrowed_date, return_plan_date, return_actual_date, description, updated, created) VALUES (1, 1, '2023-01-19', '2023-01-20', '2023-01-20', 'mượn tẹo', null, '2023-01-19 00:49:27');

INSERT INTO gr09_dev.books_transactions (book_id, user_id, borrowed_date, return_plan_date, return_actual_date, description, updated, created) VALUES (2, 2, '2023-01-19', '2023-01-20', '2023-01-20', 'mượn tẹo', null, '2023-01-19 00:49:27');

INSERT INTO gr09_dev.books_transactions (book_id, user_id, borrowed_date, return_plan_date, return_actual_date, description, updated, created) VALUES (3, 3, '2023-01-19', '2023-01-20', '2023-01-20', 'mượn tẹo', null, '2023-01-19 00:49:27');

INSERT INTO gr09_dev.books_transactions (book_id, user_id, borrowed_date, return_plan_date, return_actual_date, description, updated, created) VALUES (4, 4, '2023-01-19', '2023-01-20', '2023-01-20', 'mượn tẹo', null, '2023-01-19 00:49:27');

INSERT INTO gr09_dev.user (type, name, login_id, avatar, description, updated, created) VALUES (1, 'user1', 'a', './web/image/book/test.jpg', 'user test', null, '2023-01-19 00:47:24');
INSERT INTO gr09_dev.user (type, name, login_id, avatar, description, updated, created) VALUES (1, 'user2', 'b', './web/image/book/test.jpg', 'user test', null, '2023-01-19 00:47:24');
INSERT INTO gr09_dev.user (type, name, login_id, avatar, description, updated, created) VALUES (0, 'user3', 'c', './web/image/book/test.jpg', 'admin test', null, '2023-01-19 00:47:24');
INSERT INTO gr09_dev.user (type, name, login_id, avatar, description, updated, created) VALUES (0, 'user4', 'd', './web/image/book/test.jpg', 'admin test', null, '2023-01-19 00:47:24');
