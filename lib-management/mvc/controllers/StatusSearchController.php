<?php
class StatusSearchController extends BaseController
{

    function Default ()
    {
        $this->view("Home");
    }

    function searchAllStatusBook()
    {
        //trigger : http://localhost/gr09_library-management/lib-management/StatusSearch/searchAllStatusBook
        $modelTransaction = $this->model("Book_Transactions");
        $res = $modelTransaction->searchAllStatusBook();
        $modelUser = $this->model("User");
        $resUser = $modelUser->searchAllUser();
        $modelBook = $this->model("Book");
        $resBook = $modelBook->searchAllBook();
        $book_transactions = [];
        $usersearch_book_transactions = [""];
        $booksearch_book_transactions = [""];
        $rowNum = 1;
        while ($row = $res->fetch_assoc()) {
            $status = "";
            if ($row['Ngày trả thực tế'] != null) {
                $status = "Đã trả";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] >= date("Y-m-d")) {
                $status = "Đang mượn";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] < date("Y-m-d")) {
                $status = "Quá hạn";
            }
            array_push($book_transactions, array(
                'No' => $rowNum,
                'id' => $row['id'],
                'Tên sách' => $row['Tên sách'],
                'Người dùng' => $row['Người dùng'],
                'Tình trạng mượn' => $status,
            )
            );
            $rowNum++;
        }
        while ($row = mysqli_fetch_assoc($resUser)) {
            array_push($usersearch_book_transactions, $row['name']);
        }
        while ($row = mysqli_fetch_assoc($resBook)) {
            array_push($booksearch_book_transactions, $row['name']);
        }
        $this->views("StatusBooksLedger", $book_transactions, $usersearch_book_transactions, $booksearch_book_transactions);
    }

    function searchStatusBook($searchuser, $searchbook)
    {
        //trigger : http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBook/nameuser/namebook
        $modelTransaction = $this->model("Book_Transactions");
        $res = $modelTransaction->searchStatusBook($searchuser, $searchbook);
        $modelUser = $this->model("User");
        $resUser = $modelUser->searchAllUser();
        $modelBook = $this->model("Book");
        $resBook = $modelBook->searchAllBook();
        $book_transactions = [];
        $usersearch_book_transactions = [$searchuser];
        $booksearch_book_transactions = [$searchbook];
        $rowNum = 1;
        while ($row = mysqli_fetch_assoc($res)) {
            $status = "";
            if ($row['Ngày trả thực tế'] != null) {
                $status = "Đã trả";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] >= date("Y-m-d")) {
                $status = "Đang mượn";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] < date("Y-m-d")) {
                $status = "Quá hạn";
            }
            array_push($book_transactions, array(
                'No' => $rowNum,
                'id' => $row['id'],
                'Tên sách' => $row['Tên sách'],
                'Người dùng' => $row['Người dùng'],
                'Tình trạng mượn' => $status,
            )
            );
            $rowNum++;
        }
        while ($row = mysqli_fetch_assoc($resUser)) {
            array_push($usersearch_book_transactions, $row['name']);
        }
        array_push($usersearch_book_transactions, "");
        while ($row = mysqli_fetch_assoc($resBook)) {
            array_push($booksearch_book_transactions, $row['name']);
        }
        array_push($booksearch_book_transactions, "");
        $this->views("StatusBooksLedger", $book_transactions, $usersearch_book_transactions, $booksearch_book_transactions);
    }

    function searchStatusBookByUser($searchuser)
    {
        //trigger : http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBookByUser/nameuser
        $modelTransaction = $this->model("Book_Transactions");
        $modelUser = $this->model("User");
        $resUser = $modelUser->searchAllUser();
        $res = $modelTransaction->searchStatusBookByUser($searchuser);
        $modelBook = $this->model("Book");
        $resBook = $modelBook->searchAllBook();
        $book_transactions = [];
        $usersearch_book_transactions = [$searchuser];
        $booksearch_book_transactions = [""];
        $rowNum = 1;
        while ($row = mysqli_fetch_assoc($res)) {
            $status = "";
            if ($row['Ngày trả thực tế'] != null) {
                $status = "Đã trả";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] >= date("Y-m-d")) {
                $status = "Đang mượn";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] < date("Y-m-d")) {
                $status = "Quá hạn";
            }
            array_push($book_transactions, array(
                'No' => $rowNum,
                'id' => $row['id'],
                'Tên sách' => $row['Tên sách'],
                'Người dùng' => $row['Người dùng'],
                'Tình trạng mượn' => $status,
            )
            );
            $rowNum++;
        }
        while ($row = mysqli_fetch_assoc($resUser)) {
            array_push($usersearch_book_transactions, $row['name']);
        }
        array_push($usersearch_book_transactions, "");
        while ($row = mysqli_fetch_assoc($resBook)) {
            array_push($booksearch_book_transactions, $row['name']);
        }
        $this->views("StatusBooksLedger", $book_transactions, $usersearch_book_transactions, $booksearch_book_transactions);
    }

    function searchStatusBookByBook($searchbook)
    {
        //trigger : http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBookByBook/namebook
        $modelTransaction = $this->model("Book_Transactions");
        $modelBook = $this->model("Book");
        $modelUser = $this->model("User");
        $resUser = $modelUser->searchAllUser();
        $resBook = $modelBook->searchAllBook($searchbook);
        $res = $modelTransaction->searchStatusBookByBook($searchbook);
        $book_transactions = [];
        $usersearch_book_transactions = [""];
        $booksearch_book_transactions = [$searchbook];
        $rowNum = 1;
        while ($row = mysqli_fetch_assoc($res)) {
            $status = "";
            if ($row['Ngày trả thực tế'] != null) {
                $status = "Đã trả";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] >= date("Y-m-d")) {
                $status = "Đang mượn";
            } else if ($row['Ngày trả thực tế'] == null && $row['Ngày dự định trả'] < date("Y-m-d")) {
                $status = "Quá hạn";
            }
            array_push($book_transactions, array(
                'No' => $rowNum,
                'id' => $row['id'],
                'Tên sách' => $row['Tên sách'],
                'Người dùng' => $row['Người dùng'],
                'Tình trạng mượn' => $status,
            )
            );
            $rowNum++;
        }
        while ($row = mysqli_fetch_assoc($resBook)) {
            array_push($booksearch_book_transactions, $row['name']);
        }
        array_push($booksearch_book_transactions, "");
        while ($row = mysqli_fetch_assoc($resUser)) {
            array_push($usersearch_book_transactions, $row['name']);
        }
        $this->views("StatusBooksLedger", $book_transactions, $usersearch_book_transactions, $booksearch_book_transactions);
    }

    function test_api()
    {
        $mock_data = ["name" => "minh", "tai san" => "rat nhieu", "dep trai" => "nhat lop"];
        echo json_encode($mock_data);
    }
}