<!--
  @ Author: Minh Dang
  @ Create Time: 2023-01-09 13:46:15
  @ Description: Working hard improves my quality of life ^^
 -->

<?php
require_once('./mvc/core/Authentication.php');
class LoginController extends BaseController
{

    /** Is logged in */
    private $isLoggedIn = false;

    function Default()
    {
        require_once('./mvc/core/Authentication.php');

        // Check if user is logged in
        if (user_is_login()) {
            // Redirect to Home if authenticated
            $this->view("DefaultHome", [
                "id" => $_SESSION["id"],
                "role" => $_SESSION["role"],
                "login_date" => date("Y-m-d"),
                "login_time" => date("h:i a")
            ]);
        } else {
            $this->view("Login");
        }
    }

    /**
     * URL : http://localhost/gr09_library-management/lib-management/Login
     * 
     * @param string $loginId
     * @param string $password
     */
    function authenticate($loginId, $password)
    {
        // Else, try to authenticate
        $admins = $this->model("Admin");

        // Get matched admin
        $admin = $admins->getAdmin($loginId, $password);

        if (strcmp($admin->getPassword(), $password) === 0) {
            $_SESSION['id'] = $admin->getId();
            $_SESSION['role'] = 'Admin';

            // If passwords are matched, redirect to Home
            header("Location: http://localhost/gr09_library-management/lib-management/Home/Default");
        } else {
            // Else, redirect to Login
            $this->view("Login", [
                "err" => 'LoginID và Pasword không đúng'
            ]);
        }
    }

    /**
     * Logout
     */
    function logout()
    {
        session_destroy();
        $_SESSION = [];

        header("Location: http://localhost/gr09_library-management/lib-management/Login/Default");
    }
}
