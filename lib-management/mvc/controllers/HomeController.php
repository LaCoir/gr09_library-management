<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-29 21:46:39
  @ Description: Working hard improves my quality of life ^^
 -->
<?php
class HomeController extends BaseController{
    function Default(){
        require_once('./mvc/core/Authentication.php');
        // trigger : http://localhost/gr09_library-management/lib-management/Home/Default
        $_SESSION["id"] = 123;
        $_SESSION["role"] = "Admin";
        $check = user_is_login();
        if($check===true){
            $this->view("DefaultHome", [
                "id"=>$_SESSION["id"],
                "role"=>$_SESSION["role"],
                "login_date"=>date("Y-m-d"),
                "login_time"=>date("h:i a")
            ]);
        }else{
            echo "Login";
        }

    }

    function test_api(){
        $mock_data = ["name" => "minh", "tai san" => "rat nhieu", "dep trai" => "nhat lop"];
        echo json_encode($mock_data);
    }
}
?>