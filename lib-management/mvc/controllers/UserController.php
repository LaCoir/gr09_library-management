<?php
require_once('./mvc/core/Authentication.php');
class UserController extends BaseController{
    private $check = false; //variable check is login 
    
    function update($id){
        //trigger : http://localhost/gr09_library-management/lib-management/User/update/1
        $_SESSION["id"] = 123; //fake id to passed login 
        $_SESSION["role"] = "Admin";  //fake role to passed login 
        $this->check = user_is_login();
        if ($this->check === true) {
            $model = $this->model("User");
            $type = $model->findTypeDistance();
            $this->view("UpdateUser",[
                "id"=>$id,
                "types"=>$type]);
        }else{
            header("login.php");
        }
    }

    function confirmUpdate($id,$user_name,$user_Id,$cache_dir_avatar,
    $description){
        $this->check = user_is_login();
        if ($this->check === true) {
            $arr = [
                "id" => $id,
                "user_name" => $user_name,
                //  "user_category" => $user_category,
                "user_Id" => $user_Id,
                "cache_dir_avatar" => $cache_dir_avatar,
                "description" => $description
            ];

            $this->view("UpdateUserConfirm",$arr);
        }else{
            header("login.php");
        }
    }

    function updateSuccess($id,$user_name,
        $user_Id,$avatar_dir,
        $description){
        $this->check = user_is_login();
        $model = $this->model("User");
        if ($this->check === true) {
            $model->updateUser(
                $id,
                $user_name,
                //  $user_category,
                $user_Id,
                $description,
                str_replace("-","/",$avatar_dir)
            );
            $this->view("UpdateUserSuccess");
        }else{
            header("login.php");
        }

    }

    function test_api(){
        $mock_data = ["name" => "minh", "tai san" => "rat nhieu", "dep trai" => "nhat lop"];
        echo json_encode($mock_data);
    }
}