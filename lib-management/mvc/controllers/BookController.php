<!--
  @ Author: Minh Dang
  @ Create Time: 2023-01-09 13:46:15
  @ Description: Working hard improves my quality of life ^^
 -->

 <?php
 require_once('./mvc/core/Authentication.php');
class BookController extends BaseController{

    private $check = false; //variable check is login 

    function update($id){
        //trigger : http://localhost/duc/gr09_library-management/lib-management/SubjectController/update/1
        $_SESSION["id"] = 123; //fake id to passed login 
        $_SESSION["role"] = "Admin";  //fake role to passed login 
        $this->check = user_is_login();
        if ($this->check === true) {
            $model = $this->model("Book");
            $category = $model->findCategoryDistance();
            $author = $model->findAuthorDistance();
            $this->view("UpdateBook", [
                "id" => $id,
                "categorys" => $category,
                "authors" => $author
            ]);
        }else{
            header("LoginController.php");
        }
    }

    function confirmUpdate($id,$book_name,
        $book_category,$author,$number_of_books,
        $description,$cache_dir_avatar){
        $this->check = user_is_login();
        if ($this->check === true) {
            $arr = [
                "id" => $id,
                "book_name" => $book_name,
                "book_category" => $book_category,
                "author" => $author,
                "number_of_books" => $number_of_books,
                "description" => $description,
                "cache_dir_avatar" => $cache_dir_avatar
            ];
            $this->view("UpdateBookConfirm",$arr);
        }else{
            header("LoginController.php");
        }
    }

    function updateSuccess($id,$book_name,
        $book_category,$author,$number_of_books,
        $description,$avatar_dir){
        $this->check = user_is_login();
        $model = $this->model("Book");
        $this->check = user_is_login();
        if ($this->check === true) {
            $model->updateBook(
                $id,
                $book_name,
                $book_category,
                $author,
                $number_of_books,
                $description,
                str_replace("-","/",$avatar_dir)
            );
            $this->view("UpdateSuccess");
        }else{
            header("LoginController.php");
        }
    }
    function deleteBook($id){
//        $_SESSION["id"] = 123; //fake id to passed login
//        $_SESSION["role"] = "Admin";  //fake role to passed login
        $this->check = user_is_login();
        if ($this->check === true) {
            $model = $this->model("Book");
            $res = $model->deleteBook($id);
            $this->view("BookDelete");
        }else{
            header("LoginController.php");
        }
    }

    function findAll(){
        //trigger : http://localhost/gr09_library-management/lib-management/Book/findAll
//        $_SESSION["id"] = 123; //fake id to passed login
//        $_SESSION["role"] = "Admin";  //fake role to passed login
        $this->check = user_is_login();
        if ($this->check === true) {
            $model = $this->model("Book");
            $res = $model->allBook();
            $data = [];
            $rowNum = 1;
            while ($row = mysqli_fetch_array($res,MYSQLI_ASSOC)) {
                $data[] = array(
                    'No' => $rowNum,
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'author' => $row['author'],
                    'quantity' => $row['quantity'],
                    'category' => $row['category'],
                    'description' => $row['description'],
                );
                $rowNum++;
            }
            $this->view("BookSearch",$data);
        }else{
            header("LoginController.php");
        }
    }

    function search($cate, $key){
        //trigger : http://localhost/gr09_library-management/lib-management/Book/search/Manga/l
//        $_SESSION["id"] = 123; //fake id to passed login
//        $_SESSION["role"] = "Admin";  //fake role to passed login
        $this->check = user_is_login();
        if ($this->check === true) {
            $books = $this->model("Book");
            $res = $books->searchBook($cate,$key);
            $data = [];
            $rowNum = 1;
            while ($row = mysqli_fetch_assoc($res)){
                $data[] = array(
                    'No' => $rowNum,
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'author' => $row['author'],
                    'quantity' => $row['quantity'],
                    'category' => $row['category'],
                    'description' => $row['description'],
                );
                $rowNum++;
            }
            $this->view("BookSearch",$data);
        }else{
            header("LoginController.php");
        }
    }

    function searchByTitle($cate){
        //trigger : http://localhost/gr09_library-management/lib-management/Book/searchByTitle/Manga
//        $_SESSION["id"] = 123; //fake id to passed login
//        $_SESSION["role"] = "Admin";  //fake role to passed login
        $this->check = user_is_login();
        if ($this->check === true) {
            $books = $this->model("Book");
            $res = $books->searchBookByTitle($cate);
            $data = [];
            $rowNum = 1;
            while ($row = mysqli_fetch_assoc($res)){
                $data[] = array(
                    'No' => $rowNum,
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'author' => $row['author'],
                    'quantity' => $row['quantity'],
                    'category' => $row['category'],
                    'description' => $row['description'],
                );
                $rowNum++;
            }
            $this->view("BookSearch",$data);
        }else{
            header("LoginController.php");
        }
    }


    function create(){
        //trigger : http://localhost/gr09_library-management/lib-management/Book/create
        $this->check = user_is_login();
        if ($this->check === true) {
        $model = $this->model("Book");
        $category = $model->findCategoryDistance();
        // $author = $model->createAuthorDistance();
        $this->view("CreateBook",[
            "categorys"=>$category]);
        }else{
            header("login.php");
        }
    }

    function confirmCreate($book_name,
        $book_category,$author,$number_of_books,
        $description,$cache_dir_avatar){
            $this->check = user_is_login();
            if ($this->check === true) {
                $arr = [
                    "book_name" => $book_name,
                    "book_category" => $book_category,
                    "author" => $author,
                    "number_of_books" => $number_of_books,
                    "description" => $description,
                    "cache_dir_avatar" => $cache_dir_avatar
                ];

                $this->view("CreateBookConfirm",$arr);
            }else{
                header("login.php");
            }
    }
    
    function createSuccess($book_name,
        $book_category,$author,$number_of_books,
        $description,$avatar_dir){
            $model = $this->model("Book");
            $this->check = user_is_login();
            if ($this->check === true) {
            $model->createBook(
                $book_name,
                $book_category,
                $author,
                $number_of_books,
                $description,
                str_replace("-","/",$avatar_dir)
            );
            $this->view("CreateBookSuccess");
            }else{
                header("login.php");
             }
        }

    // update time return book
    function returnBook($id){
        $_SESSION["id"] = 123; //fake id to passed login 
        $_SESSION["role"] = "Admin";  //fake role to passed login 
        $this->check = user_is_login();
        if ($this->check === true) {
            $model = $this->model("Book_Transactions");
            $res = $model->returnBook($id);
            $this->view("ReturnBookSuccess");
        }else{
            header("LoginController.php");
        }
    }
}
