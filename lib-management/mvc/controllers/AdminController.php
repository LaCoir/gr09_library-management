<?php
require_once('./mvc/core/Authentication.php');
class AdminController extends BaseController
{
    private $check = false;
    function forgotPassword()
    {
        $this->view("ForgotPassword");
    }

    function resetPasswordRequest($login_id, $password)
    {
        $model = $this->model('Admin');
        $model->resetPasswordRequest($login_id, $password);
        $this->view("resetPasswordRequestConfirm");
    }

    function resetPassword(){
        $this->check = true;
        if($this->check){
            if($_SESSION["role"] == "Admin"){
                $this->view('ResetPassWord');
            }else{
                $this->view("Authen");
            }
        }
    }

    function resetPasswordConfirm($login_id, $reset_password_token){
        $model = $this->model('Admin');
        $model->resetPassWord($login_id, $reset_password_token);
        $this->view('ResetPasswordConfirm');

    }
    function test_api()
    {
        $model = $this->model('Admin');
        $data = $model->findAdminByLogin_Id('a');
        while ($row = mysqli_fetch_array($data)) {
            echo $row["id"];
        };
    }
}
