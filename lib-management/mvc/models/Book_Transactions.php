<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 10:09:54
  @ Description: Working hard improves my quality of life ^^
 -->
<?php

require_once('./mvc/core/ConnectionDB.php');

class Book_Transactions extends ConnectionDB{
    private $id;
    private $book_id;
    private $user_id;
    private $bomoved_date;
    private $return_plan_date;
    private $return_actual_date;
    private $descrption;
    private $updated;
    private $created;


	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param mixed $id 
	 * @return self
	 */
	public function setId($id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBook_id() {
		return $this->book_id;
	}
	
	/**
	 * @param mixed $book_id 
	 * @return self
	 */
	public function setBook_id($book_id): self {
		$this->book_id = $book_id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUser_id() {
		return $this->user_id;
	}
	
	/**
	 * @param mixed $user_id 
	 * @return self
	 */
	public function setUser_id($user_id): self {
		$this->user_id = $user_id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBomoved_date() {
		return $this->bomoved_date;
	}
	
	/**
	 * @param mixed $bomoved_date 
	 * @return self
	 */
	public function setBomoved_date($bomoved_date): self {
		$this->bomoved_date = $bomoved_date;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getReturn_plan_date() {
		return $this->return_plan_date;
	}
	
	/**
	 * @param mixed $return_plan_date 
	 * @return self
	 */
	public function setReturn_plan_date($return_plan_date): self {
		$this->return_plan_date = $return_plan_date;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getReturn_actual_date() {
		return $this->return_actual_date;
	}
	
	/**
	 * @param mixed $return_actual_date 
	 * @return self
	 */
	public function setReturn_actual_date($return_actual_date): self {
		$this->return_actual_date = $return_actual_date;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDescrption() {
		return $this->descrption;
	}
	
	/**
	 * @param mixed $descrption 
	 * @return self
	 */
	public function setDescrption($descrption): self {
		$this->descrption = $descrption;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUpdated() {
		return $this->updated;
	}
	
	/**
	 * @param mixed $updated 
	 * @return self
	 */
	public function setUpdated($updated): self {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * @param mixed $created 
	 * @return self
	 */
	public function setCreated($created): self {
		$this->created = $created;
		return $this;
	}

	public function searchAllStatusBook(){
		$_query = "SELECT books_transactions.id, user.name AS 'Người dùng', book.name AS 'Tên sách', books_transactions.return_actual_date AS 'Ngày trả thực tế', books_transactions.return_plan_date AS 'Ngày dự định trả'
		FROM books_transactions, user, book
		WHERE books_transactions.user_id = user.id AND books_transactions.book_id = book.id
		ORDER BY id DESC";
		return $this->con->query($_query);
	}

	public function searchStatusBook($searchuser, $searchbook){
		$_query = "SELECT books_transactions.id, user.name AS 'Người dùng', book.name AS 'Tên sách', books_transactions.return_actual_date AS 'Ngày trả thực tế', books_transactions.return_plan_date AS 'Ngày dự định trả'
		FROM books_transactions, user, book
		WHERE books_transactions.user_id = user.id AND books_transactions.book_id = book.id AND book.name LIKE '%$searchbook%' AND user.name LIKE '%$searchuser%'
		ORDER BY id DESC";
		return $this->con->query($_query);
	}

	public function searchStatusBookByBook($searchbook){
		$_query = "SELECT books_transactions.id, user.name AS 'Người dùng', book.name AS 'Tên sách', books_transactions.return_actual_date AS 'Ngày trả thực tế', books_transactions.return_plan_date AS 'Ngày dự định trả'
		FROM books_transactions, user, book
		WHERE books_transactions.user_id = user.id AND books_transactions.book_id = book.id AND book.name LIKE '%$searchbook%'
		ORDER BY id DESC";
		return $this->con->query($_query);
	}

	public function searchStatusBookByUser($searchuser){
		$_query = "SELECT books_transactions.id, user.name AS 'Người dùng', book.name AS 'Tên sách', books_transactions.return_actual_date AS 'Ngày trả thực tế', books_transactions.return_plan_date AS 'Ngày dự định trả'
		FROM books_transactions, user, book
		WHERE books_transactions.user_id = user.id AND books_transactions.book_id = book.id AND user.name LIKE '%$searchuser%'
		ORDER BY id DESC";
		return $this->con->query($_query);
	}

	public function returnBook($id){
		$_query = "UPDATE books_transactions
		SET return_actual_date = CURRENT_DATE
		WHERE id = $id";
		$this->con->query($_query);
	}
}

?>