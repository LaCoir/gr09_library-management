<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 09:53:15
  @ Description: Working hard improves my quality of life ^^
 -->

<?php
require_once('./mvc/core/ConnectionDB.php');

class Admin extends ConnectionDB{
    protected $id;
    protected $login_id;
    protected $pass;
    protected $active_fla;
    protected $reset_password_token;
    protected $updated;
    protected $created;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id 
	 * @return self
	 */
	public function setId($id): self
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLogin_id()
	{
		return $this->login_id;
	}

	/**
	 * @param mixed $login_id 
	 * @return self
	 */
	public function setLogin_id($login_id): self
	{
		$this->login_id = $login_id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPassword()
	{
		return $this->pass;
	}

	/**
	 * @param mixed $password 
	 * @return self
	 */
	public function setPassword($password): self
	{
		$this->pass = $password;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getActive_fla()
	{
		return $this->active_fla;
	}

	/**
	 * @param mixed $active_fla 
	 * @return self
	 */
	public function setActive_fla($active_fla): self
	{
		$this->active_fla = $active_fla;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getReset_password_token()
	{
		return $this->reset_password_token;
	}

	/**
	 * @param mixed $reset_password_token 
	 * @return self
	 */
	public function setReset_password_token($reset_password_token): self
	{
		$this->reset_password_token = $reset_password_token;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUpdated()
	{
		return $this->updated;
	}

	/**
	 * @param mixed $updated 
	 * @return self
	 */
	public function setUpdated($updated): self
	{
		$this->updated = $updated;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param mixed $created 
	 * @return self
	 */
	public function setCreated($created): self
	{
		$this->created = $created;
		return $this;
	}
	public function findAdminByLogin_Id($login_id)
     {
         $_query = "SELECT * FROM `admin` Where login_id = '$login_id'";
         return $this->con->query($_query);
     }

	public function resetPasswordRequest($login_id, $password){
		$_query = "UPDATE `admin` SET reset_password_token = '$password' Where login_id = '$login_id'";
		$this->con->query($_query);
	}

	public function findAdminByResetPassWord(){
		$_query = "SELECT * FROM `admin` Where reset_password_token != ''";
		return $this->con->query($_query);
	}

	public function resetPassWord($login_id, $reset_password_token){
		$_query = "UPDATE `admin` SET password = '$reset_password_token' Where login_id = '$login_id'";
		$_query1 = "UPDATE `admin` SET reset_password_token = '' Where login_id = '$login_id'";
		$this->con->query($_query);
		$this->con->query($_query1);
	}

	/**
	 * Get user
	 * 
	 * @param string $loginId
	 * @return Admin
	 */
	public function getAdmin($loginId)
	{
		// Create prepared statement
		$stmt = $this->con->prepare('SELECT * FROM admin WHERE login_id = ?');
		// Binding
		$stmt->bind_param("s", $loginId);
		// Execute query
		$stmt->execute();
		// Get result
		$sql = $stmt->get_result();

		if ($sql->num_rows > 0) {
			// Fetch record
			$row = $sql->fetch_assoc();

			$this->id = $row['id'];
			$this->login_id = $row['login_id'];
			$this->pass = $row['password'];
			$this->active_fla = $row['active_flag'];
			$this->reset_password_token = $row['reset_password_token'];
			$this->updated = $row['updated'];
			$this->created = $row['created'];
		} else {
			$this->id = false;
		}

		return $this;
	}
}
?>
