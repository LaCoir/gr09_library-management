<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 10:04:02
  @ Description: Working hard improves my quality of life ^^
 -->

<?php

require_once('./mvc/core/ConnectionDB.php');

class User extends ConnectionDB{
    private $id;
    private $type;
    private $name;
    private $id_login;
    private $avatar;
    private $decription;
    private $updated;
    private $created;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param mixed $id 
	 * @return self
	 */
	public function setId($id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function gettype() {
		return $this->type;
	}
	
	/**
	 * @param mixed $type 
	 * @return self
	 */
	public function settype($type): self {
		$this->type = $type;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * @param mixed $name 
	 * @return self
	 */
	public function setName($name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getId_login() {
		return $this->id_login;
	}
	
	/**
	 * @param mixed $id_login 
	 * @return self
	 */
	public function setId_login($id_login): self {
		$this->id_login = $id_login;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAvatar() {
		return $this->avatar;
	}
	
	/**
	 * @param mixed $avatar 
	 * @return self
	 */
	public function setAvatar($avatar): self {
		$this->avatar = $avatar;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDecription() {
		return $this->decription;
	}
	
	/**
	 * @param mixed $decription 
	 * @return self
	 */
	public function setDecription($decription): self {
		$this->decription = $decription;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUpdated() {
		return $this->updated;
	}
	
	/**
	 * @param mixed $updated 
	 * @return self
	 */
	public function setUpdated($updated): self {
		$this->updated = $updated;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * @param mixed $created 
	 * @return self
	 */
	public function setCreated($created): self {
		$this->created = $created;
		return $this;
	}

	public function searchAllUser()
	{
		$querry = "SELECT user.name FROM user";
		return mysqli_query($this->con, $querry);
	}

	public function findTypeDistance(){
		$_query = "SELECT DISTINCT type FROM user";
		//  echo $this->$con;
 		return $this->con->query($_query);
	}

	public function updateUser($id,$user_name,
	$Id,$avatar,$description){
		 $_query = "UPDATE user 
		 SET name='" . $user_name . "',  id='" . $Id 
		 . "', description='" . $description . "', avatar='" . $avatar  
		 . "', updated='" . date("Y-m-d H:i:s") 
		 . "' WHERE id=" . $id;
		 $this->con->query($_query);
	} 

	public function getAllUser(){
        $_query = "SELECT name FROM user";
        return $this->con->query($_query);
	}

}
?>