<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 10:07:07
  @ Description: Working hard improves my quality of life ^^
 -->

 <?php

// namespace mvc\models;

 require_once('./mvc/core/ConnectionDB.php');


 class Book extends ConnectionDB
 {
     private $id;
     private $name;
     private $category;
     private $author;
     private $quantity;
     private $avatar;
     private $decription;
     private $updated;
     private $created;

     /**
      * @return mixed
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param mixed $id
      * @return self
      */
     public function setId($id): self
     {
         $this->id = $id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getName()
     {
         return $this->name;
     }

     /**
      * @param mixed $name
      * @return self
      */
     public function setName($name): self
     {
         $this->name = $name;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getCategory()
     {
         return $this->category;
     }

     /**
      * @param mixed $category
      * @return self
      */
     public function setCategory($category): self
     {
         $this->category = $category;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getAuthor()
     {
         return $this->author;
     }

     /**
      * @param mixed $author
      * @return self
      */
     public function setAuthor($author): self
     {
         $this->author = $author;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getAvatar()
     {
         return $this->avatar;
     }

     /**
      * @param mixed $avatar
      * @return self
      */
     public function setAvatar($avatar): self
     {
         $this->avatar = $avatar;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getQuantity()
     {
         return $this->quantity;
     }

     /**
      * @param mixed $quantity
      * @return self
      */
     public function setQuantity($quantity): self
     {
         $this->quantity = $quantity;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getDecription()
     {
         return $this->decription;
     }

     /**
      * @param mixed $decription
      * @return self
      */
     public function setDecription($decription): self
     {
         $this->decription = $decription;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getUpdated()
     {
         return $this->updated;
     }

     /**
      * @param mixed $updated
      * @return self
      */
     public function setUpdated($updated): self
     {
         $this->updated = $updated;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getCreated()
     {
         return $this->created;
     }

     /**
      * @param mixed $created
      * @return self
      */
     public function setCreated($created): self
     {
         $this->created = $created;
         return $this;
     }

     public function findCategoryDistance()
     {
         $_query = "SELECT DISTINCT category FROM book";
         //  echo $this->$con;
         return $this->con->query($_query);
     }

     public function findAuthorDistance()
     {
         $_query = "SELECT DISTINCT author FROM book";
         return $this->con->query($_query);
     }

     public function updateBook($id, $book_name,
                                $book_category, $author, $number_of_books,
                                $description, $avatar)
     {
         $_query = "UPDATE book 
		 SET name='" . $book_name . "', category='" . $book_category
             . "', author='" . $author . "', quantity='" . $number_of_books
             . "', avatar='" . $avatar . "', description='" . $description
             . "', updated='" . date("Y-m-d H:i:s")
             . "' WHERE id=" . $id;
         $this->con->query($_query);
     }

     public function allBook()
     {
         $querry = "SELECT * FROM book";
         return mysqli_query($this->con, $querry);
     }

     public function searchAllBook()
     {
         $querry = "SELECT book.name FROM book";
         return mysqli_query($this->con, $querry);
     }

     public function searchBook($cate, $key)
     {
         $querry = "SELECT * FROM book WHERE category = '$cate' AND (name LIKE '%$key%' OR description LIKE '%$key%' OR author LIKE '%$key%' OR category LIKE '%$key%')";
         return $this->con->query($querry);
     }

     public function searchBookByTitle($cate)
     {
         $querry = "SELECT * FROM book WHERE category = '$cate'";
         return $this->con->query($querry);
     }

     public function deleteBook($id)
     {
         $querry = "DELETE FROM book WHERE id = '$id'";
         return $this->con->query($querry);
     }


     public function createBook($book_name,
                                $book_category, $author, $number_of_books,
                                $description, $avatar)
     {
         $_query = "INSERT INTO book(
			name  , category , author ,quantity , 
			avatar , description , created )
			VALUES ('" . $book_name . "','" . $book_category . "','" .
             $author . "','" . $number_of_books . "','" .
             $avatar . "','" . $description . "','" .
             date("Y-m-d H:i:s") . "')";
         $this->con->query($_query);
     }
 }
 ?>