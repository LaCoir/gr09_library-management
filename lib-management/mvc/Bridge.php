<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-29 21:16:34
  @ Description: Working hard improves my quality of life ^^
 -->

<?php
    require_once "./mvc/core/App.php";
    require_once "./mvc/core/BaseController.php";
    date_default_timezone_set('Asia/Ho_Chi_Minh');
?>