<!DOCTYPE html>
<html lang="vn">
<?php

if (isset($_POST["user"]) && isset($_POST["book"])) {
    if (empty($_POST["user"]) && empty($_POST["book"])) {
        header('Location: http://localhost/gr09_library-management/lib-management/StatusSearch/searchAllStatusBook');
    } else if (empty($_POST["user"]) && !empty($_POST["book"])) {
        header('Location: http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBookByBook/'
            . $_POST["book"]);
    } else if (!empty($_POST["user"]) && empty($_POST["book"])) {
        header('Location: http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBookByUser/'
            . $_POST["user"]);
    } else {
        header('Location: http://localhost/gr09_library-management/lib-management/StatusSearch/searchStatusBook/'
            . $_POST["user"] . '/'
            . $_POST["book"]);
    }

}

?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<style>
    .title-page {
        width: 100vw;
        height: 20vh;
    }

    .text-title_page {
        padding: 60px;
        line-height: 30px;
        font-size: 30px;
        text-align: center;
    }

    input[type=text] {
        width: 182px;
        height: 34px;
        border: 2px solid #4f7ba3;
        background-color: #e1eaf4;
    }

    div.ex1 {
        width: 700px;
        height: 250px;
        margin-left: auto;
        margin-right: auto;
        padding: 25px;
        background-color: #EBEBEB;
    }

    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
        text-align: center;
    }

    select {
        width: 410px;
        height: 40px;
        margin-right: 20px;
        margin-left: 50px;
        margin-top: 30px;
        border: 2px solid #4f7ba3;
        background-color: #e1eaf4;
    }

    button {
        background-color: #4f81bd;
        border: 2px solid #385D8A;
        color: white;
        width: 120px;
        height: 40px;
        margin: auto;
        border-radius: 8px;
        margin-top: 10px;
    }

    font {
        width: 200px;
        height: 40px;
        margin: auto;
    }
</style>

<body>

    <div class="title-page">
        <div class="text-title_page">Tìm kiếm tình trạng sách</div>
    </div>

    <div class="ex1">
        <form action="" method="POST" enctype="multipart/form-data">

            <div class="container">
                <font face="Arial" style="margin-left: 30px">
                    Tên sách
                </font>
                <select name="book" id="book">
                    <?php
                    echo '<option>', $data3[0], '</option>';
                    $nameBook = array();
                    foreach ($data as $row) {
                        array_push($nameBook, $row['Tên sách']);
                    }
                    foreach ($data3 as $row) {
                        array_push($nameBook, $row);
                    }
                    $res = array_unique($nameBook);
                    foreach ($res as $row) {
                        if($row != $data3[0])
                            echo '<option value= "' . $row . '">' . $row . '</option>';
                    }
                    ?>
                </select>
            </div>

            <div class="container">
                <font face="Arial" style="margin-left: 10px">
                    Người dùng
                </font>
                <select name="user" id="user">
                    <?php
                    echo '<option>', $data2[0], '</option>';
                    $nameUser = array();
                    foreach ($data as $row) {
                        array_push($nameUser, $row['Người dùng']);
                    }
                    foreach ($data2 as $row) {
                        array_push($nameUser, $row);
                    }
                    $res = array_unique($nameUser);
                    foreach ($res as $row) {
                        if($row != $data2[0])
                            echo '<option value= "' . $row . '">' . $row . '</option>';
                    }
                    ?>
                </select>
            </div>

            <div style="display: flex;align-items: center; margin-top: 20px;">
                <button onclick="clearInput()" name="delete" id="delete" style="margin-left: 190px;">
                    Reset
                </button>

                <button name="ok" id="search" value="search" style="margin-right: 130px;">
                    Tìm kiếm
                </button>
            </div>
        </form>
    </div>

    <div style="margin: auto; margin-left: 414px; margin-top: 90px;">
        <b>Số bản ghi tìm thấy:
            <?php echo sizeof($data); ?>
        </b>
    </div>

    <table style="margin: auto; margin-top: 5px; ">
        <tr>
            <td style="width:50px;">No</td>
            <td style="width:500px;">Tên sách</td>
            <td style="width:250px;">Người dùng</td>
            <td style="width:150px;">Tình trạng mượn</td>
            <td style="width:100px;">Action</td>
        </tr>

        <?php
        foreach ($data as $row):
            if ($row['Tình trạng mượn'] == "Đã trả") {

                ?>
                <tr>
                    <td>
                        <?php echo $row['No']; ?>
                    </td>
                    <td>
                        <?php echo $row['Tên sách']; ?>
                    </td>
                    <td>
                        <?php echo $row['Người dùng']; ?>
                    </td>
                    <td>
                        <?php echo $row['Tình trạng mượn']; ?>
                    </td>
                    <td></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td>
                        <?php echo $row['No']; ?>
                    </td>
                    <td>
                        <?php echo $row['Tên sách']; ?>
                    </td>
                    <td>
                        <?php echo $row['Người dùng']; ?>
                    </td>
                    <td>
                        <?php echo $row['Tình trạng mượn']; ?>
                    </td>
                    <!-- trigger : http://localhost/gr09_library-management/lib-management/Book/returnBook/1 -->
                    <td><a href="../Book/returnBook/<?php echo $row['id']; ?>"
                            onclick="return confirm('Bạn có muốn trả sách <?php echo $row['Tên sách'] ?>?')">Trả</a></td>
                </tr>
                <?php
            }
            ?>

            <?php
        endforeach;
        ?>

    </table>

    <script>
        function clearInput() {
            const inputFieldUser = document.getElementById("user");
            const inputFieldBook = document.getElementById("book");
            inputFieldUser.value = "";
            inputFieldBook.value = "";
        }
    </script>

</body>

</html>