<html lang="vi">

<head>
  <meta charset="UTF-8">

  <!-- base style  -->
  <link rel="stylesheet" href="styles.css">

  <!-- bootstrap -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
  <title>Home</title>
</head>
<style>
    .col-sm-3 {
  background-color: #EBEBEB;
  margin-right: 20px;
  text-align: center;
}

.search-box {
  background-color: #EBEBEB;
  width: 30%;
}
.info-col{
  margin-left: 10%;
  padding: 5px;
}
/* .col-sm-3{
  margin-left: 10%;
} */
</style>
<body>
  <body class="mx-auto p-5 " >
    <div class=''>
    <div class="container-box">
      <div class="search-box">
        <form id="search-form" action="" method="get">
          <div class="info-col">
          <p>
              <?php
                echo "User: ".$data["role"];
              ?>
            </p>
          </div>
          <div class="info-col">
            <p>
              <?php
                echo "Ngày login: ".$data["login_date"];
              ?>
            </p>
          </div>
          <div class="info-col">
          <p>
              <?php
                echo "Giờ login: ".$data["login_time"];
              ?>
            </p>
          </div>
        </form>
      </div>
    </br>
    <div class="container">
      <div class="row align-items-start">
        <div class="col-sm-3">
         <p> Người dùng</p>
         <p><a href="#">Thêm mới</a></p>
         <p><a href="#">Tìm kiếm</a></p>
         <p><a href="#">Lịch sử mượn</a></p>
         <p><a href="http://localhost/gr09_library-management/lib-management/Admin/resetPassword">ResetPassword</a></p>
        </div>
        <div class="col-sm-3">
          <p>Sách</p>
          <p><a href="http://localhost/gr09_library-management/lib-management/Book/create">Thêm mới</a></p>
          <p><a href="http://localhost/gr09_library-management/lib-management/Book/findAll">Tìm kiếm</a></p>
          <p><a href="#">Lịch sử mượn</a></p>
          <p><br/></p>
        </div>
        <div class="col-sm-3">
          <P>Mượn/trả sách</p>
          <p><a href="#">Mượn sách</a></p>
          <p><a href="http://localhost/gr09_library-management/lib-management/StatusSearch/searchAllStatusBook">Số cái mượn/trả sách</a></p>
          <p><a href="#">Tìm kiếm nâng cao</a></p>
          <p><br/></p>
          <p></p>
        </div>
      </div>
    </div>
     
    </div>
</div>
  </body>
</html>