<?php
require_once("./mvc/models/Admin.php");
$errorUserName = "";
$errorPassword = "";
$count = 0;
if (isset($_POST["btn"])) {
    $userName = $_POST["userName"];
    $password = $_POST["password"];
    $model = new Admin;
    $res = $model->findAdminByLogin_Id($userName);
    if($userName == ""){
        $errorUserName = "Tên đăng nhập không thể để trống!";
        $count++;
    }
    
    if($password == ""){
        $errorPassword = "Mật khẩu không thể để trống!";
        $count++;
    }

    if(strlen($password)<8 || strlen($password)>16 ){
        $errorPassword = "Mật khẩu phải từ 8 đến 16 kí tự!";
        $count++;
    }

    if(mysqli_num_rows($res) == 0){
        $errorUserName = "Tên đăng nhập không tồn tại!";
        $count++;
    }

    if($count == 0){
        header('Location: http://localhost/gr09_library-management/lib-management/Admin/resetPasswordRequest/' . $userName . '/' . $password);
    }

}
?>

<html>
<head>
    <meta charset='UTF-8'>
    <style>
        *{
            box-sizing: border-box;
        }
        body{
            margin: 0;
            padding: 0;
        }
        .forgotPass-content{
            width: 800px;
            height: 250px;
            background-color: #EBEBEB;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 0 auto;
            margin-top: 100px;
        }
        .forgotPass-input{
            width: 650px;
            height: 60px;
            display: flex;
            justify-content: space-around;
            align-items: center;
        }
        .forgotPass-input input{
            width: 500px;
            height: 50px;
            font-size: 20;
            border-radius: 8px;
            padding-left: 12px;
        }
        .forgotPass-input label{
            font-size: 20;
        }
        span{
            margin-left: 150px;
            color: red;
        }
        .forgotPass-btn{
            width: 650px;
            height: 40px;
            display: flex;
            justify-content: center;
            margin-top: 15px;
        }

        .forgotPass-btn input{
            width: 350px;
            height: 40px;
            background-color: #F5F5F5;
            cursor: pointer;
            border-radius: 8px;
        }
        .forgotPass-btn input:hover{
            background-color: wheat;
        }
    </style>
</head>
<body>
    <div class="forgotPass-content">
        <form action="" method="post">
            <div class="forgotPass-input">
                <label for="">Tên đăng nhập:</label><input type="text" name="userName">
            </div>
            <?php
                if($errorUserName != ""){
                    echo "<span>".$errorUserName."</span>";
                };
            ?>
            <div class="forgotPass-input">
                <label for="">Mật khẩu mới:</label><input type="password" name="password">
            </div>
            <?php
                if($errorPassword != ""){
                    echo "<span>".$errorPassword."</span>";
                };
            ?>
            <div class="forgotPass-btn">
                <input type="submit" value="Gửi yêu cầu reset password" name="btn">
            </div>
        </form>
    </div>
</body>
</html>