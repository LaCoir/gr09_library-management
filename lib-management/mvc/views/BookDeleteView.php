<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    body{
        margin: 0px;
    }
    .parent {
        height: 100vh;
        width: 100vw;
        position: relative;
    }
    .main-message{
        background: #EBEBEB;
        width: 400px;
        height: 80px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -50px 0 0 -200px;
        justify-content: center;
    }
    .content{
        text-align: center;
    }
</style>
<body>
<div class="parent">
    <div class="main-message">
        <p class="content">Bạn đã xóa sách thành công</p>
        <a class="content" href="http://localhost/gr09_library-management/lib-management/Book/findAll">Quay lại màn hình tìm kiếm</a>
    </div>
</div>

</body>
</html>
