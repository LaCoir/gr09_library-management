<?php
require_once("./mvc/models/Admin.php");
$model = new Admin;
$data = $model->findAdminByResetPassWord();
$count = 1;
?>
<html>

<head>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            padding: 0;
        }

        .header {
            width: 100vw;
            height: 50px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .header a {
            margin-left: 25px;
        }

        .content {
            width: 100vw;
            margin-top: 50px;
            display: flex;
            justify-content: center;
        }

        .noti {
            color: blue;
        }

        th{
            padding: 14px 24px;
        }
        td{
            text-align: center;
            padding: 10px 0;
        }

        button{
            border: none;
        }
        button a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class="header">
        <a href="http://localhost/gr09_library-management/lib-management/Home/Default#">Quay lại</a>
        <h1>Reset Password</h2>
            <div></div>
    </div>
    <div class="content">
        <?php
        if (mysqli_num_rows($data) == 0) {
            echo '<h1 class = "noti"> Không có yêu cầu reset password </h1>';
        } else {
            echo '<table border = "1">';
            echo '<thread>';
            echo '<tr>';
            echo '<th>No</th>';
            echo '<th>Tên người dùng</th>';
            echo '<th>Mật khẩu mới</th>';
            echo '<th>Action</th>';
            echo '</tr>';
            echo '</thread>';
            echo '<tbody>';
            while ($row = mysqli_fetch_array($data)) {
                echo '<tr>';
                echo '<td>' . $count . '</td>';
                echo '<td>' . $row["login_id"] . '</td>';
                echo '<td>' . $row["reset_password_token"] . '</td>';
                // echo '<td> <button><a href="https://webcoban.vn/html/demo?file=3149">Reset</a></button> </td>';
                echo '<td> <button><a href="http://localhost/gr09_library-management/lib-management/Admin/resetPasswordConfirm/'. $row["login_id"] . '/' .
                $row["reset_password_token"] . '">Reset</a></button> </td>';
                echo '</tr>';
            };
            echo '</tbody>';
        }
        ?>
    </div>
</body>

</html>