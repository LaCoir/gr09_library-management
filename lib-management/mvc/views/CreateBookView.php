<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>body{
      margin: 0px;
  }
  .title-page{
      width: 100vw;
      height: 20vh;
  }
  .text-title_page{
      padding: 60px;
      line-height: 30px;
      font-size: 30px;
      text-align: center;
  }
  .body{
      width: 100vw;
      height: 80vh;
  }
  .main{
      border-radius: 5px;
      padding: 15px;
      width: 800px;
      height: 500px;
      background-color: #EBEBEB;
      margin-left: 400px;
  }
  .margin-top20{
      margin-top: 20px;
  }
  * {
      box-sizing: border-box;
    }
    
    input[type=text], select, textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }
    
    label {
      padding: 12px 12px 12px 0;
      display: inline-block;
    }
    
    input[type=submit] {
      background-color: #04AA6D;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }
    
    input[type=submit]:hover {
      background-color: #45a049;
    }
    
    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }
    
    .col-25 {
      font-size: 18px;
      float: left;
      width: 25%;
    }
    
    .col-75 {
      float: left;
      width: 75%;
    }
    
    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    
    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
      }
    }</style>
</head>
<body>
    <?php
    $errBookName = "";
    $errCategory = "";
    $errAuthor = "";
    $errNumberOfBook = "";
    $errDescription = "";
    $errAvatar = "";

    $isFullDataRequire = true;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if(empty(inputHandling($_POST["book_name"]))){
        $errBookName = "Hãy nhập tên sách";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["book_name"])>100){
        $errBookName = "Không nhập quá 100 kí tự";
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["book_category"]))){
        $errCategory = "Hãy chọn thể loại";  
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["author"]))){
        $errAuthor = "Hãy nhập tên tác giả";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["author"])>250){
        $errBookName = "Không nhập quá 250 kí tự";
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["number_of_books"]))){
        $errNumberOfBook = "Hãy nhập số lượng";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["number_of_books"])>2){
        $errNumberOfBook = "Số lượng sách nhỏ hơn 100";
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["description"]))){
        $errDescription = "Hãy nhập mô tả chi tiết";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["description"])>1000){
        $errDescription = "Không nhập quá 1000 kí tự";
        $isFullDataRequire = false;
      }
      if(!isset($_FILES["avatar"])||$_FILES["avatar"]['error'] != 0){
        $errAvatar = "Hãy chọn lại avatar";  
        $isFullDataRequire = false;
      }
      $allowtypes = array('jpg', 'png', 'jpeg');
      $imageFileType = pathinfo(basename($_FILES["avatar"]["name"]),PATHINFO_EXTENSION);
      if (!in_array($imageFileType,$allowtypes )){
          $isFullDataRequire = false;
      }

      $cache_dir_avatar = "./web/image/cache/";
      if (!file_exists($cache_dir_avatar)) {
        mkdir($cache_dir_avatar, 0777, true);
      }
      $cache_dir_avatar = $cache_dir_avatar . $_FILES["avatar"]["name"];

      copy($_FILES["avatar"]["tmp_name"], $cache_dir_avatar);

      $cache_dir_avatar = str_replace("/", "-", $cache_dir_avatar);

      if($isFullDataRequire){
        $avatar = "avatar";
        $_SESSION["avatar"] = $_FILES["avatar"];
        header( 'Location: http://localhost/gr09_library-management/lib-management/Book/confirmCreate/'
        . $_POST["book_name"] . '/'
        . $_POST["book_category"] . '/'
        . $_POST["author"] . '/'
        . $_POST["number_of_books"] . '/'
        . $_POST["description"] . '/'
        . $cache_dir_avatar);  
      }
    }
    function inputHandling($data) {
      $data = trim($data);
      $data = stripslashes($data);
      return $data;
    }

    ?>
    <div class="title-page">
        <div class="text-title_page">Thêm thông tin sách</div>
    </div>
    <div class="body">
        <div class="main">
            <form method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-25">
                    <label for="book_name">Tên sách</label>
                  </div>
                  <div class="col-75">
                    <input type="text" id="book_name" name="book_name" placeholder="Ví dụ : Dế Mèn Phiêu Lưu Ký">
                    <div style="color: #FF0000;">
                      <?php
                        if($errBookName!=""){
                          echo "<div>".$errBookName."</div>";
                        }else{
                          echo "<br>";
                        }
                      ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="book_category">Thể loại</label>
                  </div>
                  <div class="col-75">
                    <select id="book_category" name="book_category" >
                      <option value=''></option>
                      <?php
                        while($row = mysqli_fetch_array($data["categorys"])){
                          echo "<option value='" . $row["category"] . "'>" . $row["category"] . "</option>";
                        }
                      ?>  
                    </select>
                    <div style="color: #FF0000;">
                    <?php
                      if($errCategory!=""){
                        echo "<div>".$errCategory."</div>";
                      }else{
                        echo "<br>";
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-25">
                      <label for="author">Tác giả</label>
                    </div>
                    <div class="col-75">
                    <input type="text" id="author" name="author" placeholder="Ví dụ : Dế Mèn Phiêu Lưu Ký">
                    <div style="color: #FF0000;">
                        <?php
                            if($errAuthor!=""){
                              echo "<div>".$errAuthor."</div>";
                            }else{
                              echo "<br>";
                            }
                          ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-25">
                      <label for="number_of_books">Số lượng</label>
                    </div>
                    <div class="col-75">
                      <input type="text" id="number_of_books" name="number_of_books" placeholder="ghi số ví dụ : 1,2,3,...">
                      <div style="color: #FF0000;">
                        <?php
                          if($errNumberOfBook!=""){
                            echo "<div>".$errNumberOfBook."</div>";
                          }else{
                            echo "<br>";
                          }
                        ?>
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-25">
                    <label for="description">Mô tả chi tiết</label>
                  </div>
                  <div class="col-75">
                    <textarea id="description" name="description" placeholder="Ví dụ : xuất bản lần đầu :1941 ,..." style="height:100px"></textarea>
                    <div style="color: #FF0000;">
                      <?php
                          if($errDescription!=""){
                            echo "<div>".$errDescription."</div>";
                          }else{
                            echo "<br>";
                          }
                        ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-25" style="margin-bottom: 0px;">
                      <label for="avatar">Avatar</label>
                    </div>
                    <div class="col-75" style="margin-bottom: 0px;">
                        <input type="file" name="avatar" id="avatar">
                        <div style="color: #FF0000;">
                          <?php
                            if($errAvatar!=""){
                              echo "<div>".$errAvatar."</div>";
                            }else{
                              echo "<br>";
                            }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-right: 300px;">
                  <input type="submit" name="submitUpdateBook" value="Xác nhận" style="background-color: gray;">
                </div>
                </form>
        </div>
    </div>
    
</body>
</html>