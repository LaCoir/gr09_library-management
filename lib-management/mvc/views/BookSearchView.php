<!DOCTYPE html>
<html lang="en">
<?php
$title = array("Khoa học", "Tiểu thuyết", "Manga", "Sách Giáo Khoa" );

$handle = true;

if(isset($_POST["search"])){
    if(empty($_POST["search"])){
        $_POST["search"] = " ";
        header('Location: http://localhost/gr09_library-management/lib-management/Book/searchByTitle/'
            . $_POST["title"]);
    }
    else {
        header('Location: http://localhost/gr09_library-management/lib-management/Book/search/'
            . $_POST["title"] . '/'
            . $_POST["search"]);
    }
}
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        td {
            text-align: center;
            font-weight: bold;
        }

        table {
            background-color: white;
            color: black;
        }

        caption {
            font-size: 30px;
            background-color: antiquewhite;
            color: black;
            font-weight: bold;
        }

        th {
            background-color: white;
            color: navy
        }

        td.center {
            background-color: white;
            color: black;
        }
    </style>
</head>
<body>
<h2 style="text-align: center">Tìm kiếm sách</h2>
<form action = "" method = "post">
        <table class="search-form" align = "center" cellpadding="5" style="background-color: antiquewhite; width: 400px">
            <tr>
                <td>
                    Thể loại
                </td>
                <td style="text-align: left">
                        <select name = 'title' id="title" style="width: 300px; height: 30px">
                            <?php
                            foreach ($title as $category){
                                echo '<option value= "'.$category.'">'.$category.'</option>';
                            }
                            ?>
                        </select>
            </tr>
            <tr>
                <td>
                    Từ khóa
                </td>
                <td style="text-align: left" >
                        <input style="width: 293px; height: 27px" type="text" name = "search" placeholder = "Nhập từ khóa cần tìm" value =
                                "<?php if(isset($_POST["search"])){echo $_POST["search"];} ?>">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <input type = "submit" value = "Tìm kiếm" align="center">
                    <a href="http://localhost/gr09_library-management/lib-management/Book/findAll">Tìm tất cả</a>
                </td>
            </tr>
        </table>
    </form>

<table border ="1" align ="center" cellspacing="0" cellpadding = "5" width = "1000px">
    <b>Số lượng sách được tìm thấy: <?php echo sizeof($data) ?></b>
    <tr>
        <th>No</th>
        <th>Tên sách</th>
        <th>Số lượng</th>
        <th>Thể loại</th>
        <th>Tác giả</th>
        <th>Mô tả chi tiết</th>
    </tr>
    <?php
    foreach ($data as $row): ?>
    <tr>
        <td><?php echo $row['No']; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['quantity']; ?></td>
        <td><?php echo $row['author']; ?></td>
        <td><?php echo $row['category']; ?></td>
        <td><?php echo $row['description']; ?></td>
        <td>
            <a href="http://localhost/gr09_library-management/lib-management/Book/update/<?php echo $row['id'];?>">Sửa</a>
            <a href="http://localhost/gr09_library-management/lib-management/Book/deleteBook/<?php echo $row['id'];?>" onclick="return confirm('Bạn có muốn xóa <?php echo $row['name']?> ?')">Delete</a>
        </td>
    </tr>
    <?php
    endforeach;
    ?>
</table>
</body>


