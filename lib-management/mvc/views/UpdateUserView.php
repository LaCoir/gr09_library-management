<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>body{
      margin: 0px;
  }
  .title-page{
      width: 100vw;
      height: 20vh;
  }
  .text-title_page{
      padding: 60px;
      line-height: 30px;
      font-size: 30px;
      text-align: center;
  }
  .body{
      width: 100vw;
      height: 80vh;
  }
  .main{
      border-radius: 5px;
      padding: 15px;
      width: 800px;
      height: 500px;
      background-color: #EBEBEB;
      margin-left: 400px;
  }
  .margin-top20{
      margin-top: 20px;
  }
  * {
      box-sizing: border-box;
    }
    
    input[type=text], select, textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }
    
    label {
      padding: 12px 12px 12px 0;
      display: inline-block;
    }
    
    input[type=submit] {
      background-color: #04AA6D;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }
    
    input[type=submit]:hover {
      background-color: #45a049;
    }
    
    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }
    
    .col-25 {
      font-size: 18px;
      float: left;
      width: 25%;
    }
    
    .col-75 {
      float: left;
      width: 75%;
    }
    
    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    
    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
      }
    }</style>
</head>
<body>
    <?php

    $class = array(
      0 => 'Giáo viên',
      1 => 'Sinh viên'
    );
    
    $errUserName = "";
    $errCategory = "";
    $errId = "";
    $errAvatar = "";
    $errDescription = "";

    $isFullDataRequire = true;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if(empty(inputHandling($_POST["user_name"]))){
        $errUserName = "Hãy nhập tên tên người dùng.";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["user_name"])>100){
        $errUserName = "Không nhập quá 100 kí tự";
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["user_category"]))){
        $errCategory = "Hãy chọn thể loại";  
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["user_Id"]))){
        $errId = "Hãy nhập Id người dùng";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["user_Id"])>250){
        $errUserName = "Không nhập quá 10 kí tự";
        $isFullDataRequire = false;
      }
      if(!isset($_FILES["avatar"])||$_FILES["avatar"]['error'] != 0){
        $errAvatar = "Hãy chọn lại avatar";  
        $isFullDataRequire = false;
      }
      if(empty(inputHandling($_POST["description"]))){
        $errDescription = "Hãy nhập mô tả chi tiết";  
        $isFullDataRequire = false;
      }elseif(strlen($_POST["description"])>1000){
        $errDescription = "Không nhập quá 1000 kí tự";
        $isFullDataRequire = false;
      }
      $allowtypes = array('jpg', 'png', 'jpeg');
      $imageFileType = pathinfo(basename($_FILES["avatar"]["name"]),PATHINFO_EXTENSION);
      if (!in_array($imageFileType,$allowtypes )){
          $isFullDataRequire = false;
      }

      if($isFullDataRequire){
        $cache_dir_avatar = "./web/image/cache/";
        if (!file_exists($cache_dir_avatar)) {
          mkdir($cache_dir_avatar, 0777, true);
        }
        $cache_dir_avatar = $cache_dir_avatar . $_FILES["avatar"]["name"];

        copy($_FILES["avatar"]["tmp_name"], $cache_dir_avatar);

        $cache_dir_avatar = str_replace("/", "-", $cache_dir_avatar);
        $avatar = "avatar";
        $_SESSION["avatar"] = $_FILES["avatar"];
        header( 'Location: http://localhost/gr09_library-management/lib-management/User/confirmUpdate/'

        . $data["id"] . '/'
        . $_POST["user_name"] . '/'
        // . $_POST["user_category"] . '/'
        . $_POST["user_Id"] . '/'
        . $cache_dir_avatar . '/'  
        . $_POST["description"]);
      }
    }
    function inputHandling($data) {
      $data = trim($data);
      $data = stripslashes($data);
      return $data;
    }

    ?>
    <div class="title-page">
        <div class="text-title_page">Sửa thông tin người dùng</div>
    </div>
    <div class="body">
        <div class="main">
            <form method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-25">
                    <label for="user_name">Họ và tên</label>
                  </div>
                  <div class="col-75">
                    <input type="text" id="user_name" name="user_name" placeholder="Ví dụ : Trần Hữu Sơn">
                    <div style="color: #FF0000;">
                      <?php
                        if($errUserName!=""){
                          echo "<div>".$errUserName."</div>";
                        }else{
                          echo "<br>";
                        }
                      ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="user_category">Phân loại</label>
                  </div>
                  <div class="col-75">
                  <div id="user_category" >
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <input id="user_category" type="radio" name="user_category" value='<?php echo $class[$i]; ?>' >
                        <?php echo $class[$i]; ?>
                    <?php
                    }
                    ?>
                </div>              
                    <div style="color: #FF0000;">
                    <?php
                      if($errCategory!=""){
                        echo "<div>".$errCategory."</div>";
                      }else{
                        echo "<br>";
                      }
                      ?>
                    </div>
                  </div>
                </div>
                  <div class="row">
                    <div class="col-25">
                      <label for="user_Id">ID</label>
                    </div>
                    <div class="col-75">
                      <input type="text" id="user_Id" name="user_Id" placeholder="ghi số ví dụ : 1123123">
                      <div style="color: #FF0000;">
                        <?php
                          if($errId!=""){
                            echo "<div>".$errId."</div>";
                          }else{
                            echo "<br>";
                          }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-25" style="margin-bottom: 0px;">
                      <label for="avatar">Avatar</label>
                    </div>
                    <div class="col-75" style="margin-bottom: 0px;">
                        <input type="file" name="avatar" id="avatar">
                        <div style="color: #FF0000;">
                          <?php
                            if($errAvatar!=""){
                              echo "<div>".$errAvatar."</div>";
                            }else{
                              echo "<br>";
                            }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="description">Mô tả chi tiết</label>
                  </div>
                  <div class="col-75">
                    <textarea id="description" name="description" placeholder="Ví dụ : có nốt ruồi ở dưới cánh." style="height:100px"></textarea>
                    <div style="color: #FF0000;">
                      <?php
                          if($errDescription!=""){
                            echo "<div>".$errDescription."</div>";
                          }else{
                            echo "<br>";
                          }
                        ?>
                    </div>
                  </div>
                </div>
                
                <div class="row" style="margin-right: 300px;">
                  <input type="submit" name="submitUpdateBook" value="Xác nhận" style="background-color: gray;">
                </div>
                </form>
        </div>
    </div>
    
</body>
</html>