<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>body{
      margin: 0px;
  }
  .title-page{
      width: 100vw;
      height: 20vh;
  }
  .text-title_page{
      padding: 60px;
      line-height: 30px;
      font-size: 30px;
      text-align: center;
  }
  .body{
      width: 100vw;
      height: 80vh;
  }
  .main{
      border-radius: 5px;
      padding: 15px;
      width: 800px;
      height: 500px;
      background-color: #EBEBEB;
      margin-left: 400px;
  }
  .margin-top20{
      margin-top: 20px;
  }
  * {
      box-sizing: border-box;
    }
    
    input[type=text], select, textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }
    
    label {
      padding: 12px 12px 12px 0;
      display: inline-block;
    }
    
    input[type=submit] {
      background-color: #04AA6D;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }
    
    input[type=submit]:hover {
      background-color: #45a049;
    }
    
    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }
    
    .col-25 {
      font-size: 18px;
      float: left;
      width: 25%;
    }
    
    .col-75 {
      float: left;
      width: 75%;
    }
    
    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    
    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
      }
    }</style>
</head>
<body>
    <?php

$class = array(
  0 => 'Giáo viên',
  1 => 'Sinh viên'
);

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

      $target_dir    = "./web/image/avatar/";
      $avatar_dir = $target_dir. $data["user_name"].".".pathinfo($data["cache_dir_avatar"], PATHINFO_EXTENSION);
      $data["cache_dir_avatar"] = str_replace("-", "/", $data["cache_dir_avatar"]);
      if (copy($data["cache_dir_avatar"],$avatar_dir)){
        // echo $data["cache_dir_avatar"] = "http://localhost/gr09_library-management/lib-management".substr($data["cache_dir_avatar"],1);
        // rmdir("../core/data/cache");
        $avatar_dir = str_replace("/", "-", $avatar_dir);
        header( 'Location: http://localhost/gr09_library-management/lib-management/User/updateSuccess/'
        . $data["id"] . '/'
        . $data["user_name"] . '/'
        // . $data["user_category"] . '/'
        . $data["user_Id"] . '/'
        . $avatar_dir . '/'  
        . $data["description"]);
      
      }
    }
    ?>
    <div class="title-page">
        <div class="text-title_page">Sửa thông tin người dùng</div>
    </div>
    <div class="body">
        <div class="main">
            <form method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-25">
                    <label for="user_name">Họ và tên</label>
                  </div>
                  <div class="col-75">
                    <label for="user_name">
                        <?php echo $data["user_name"];  ?>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="user_category">Phân loại</label>
                  </div>
                  <div class="col-75">
                    <label for="user_category">
                        <?php echo "Giáo viên"; ?>
                    </label>
                  </div>
                </div>
                <div class="row">
                    <div class="col-25">
                      <label for="Id">Id</label>
                    </div>
                    <div class="col-75">
                        <label for="Id">
                            <?php echo $data["user_Id"]; ?>
                        </label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-25" style="margin-bottom: 0px;">
                      <label for="avatar">Avatar</label>
                    </div>
                    <div class="col-75" style="margin-bottom: 0px;">
                        <?php
                        $data["cache_dir_avatar"] = str_replace("-", "/", $data["cache_dir_avatar"]);
                        echo "<img src='"."http://localhost/gr09_library-management/lib-management".substr($data["cache_dir_avatar"],1)."' width='50' height='50'>";
                        ?>
                        
                    </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="description">Mô tả chi tiết</label>
                  </div>
                  <div class="col-75">
                    <label for="description">
                        <?php echo $data["description"]; ?>
                    </label>
                  </div>
                </div>
                
                <div class="row" style="margin-right: 300px;">
                  <input type="submit" name="submitUpdateUser" value="Xác nhận" style="background-color: gray;">
                </div>
                </form>
        </div>
    </div>
    
</body>
</html>