<!DOCTYPE html>
<html lang="en">
<?php
function inputHandling($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  return $data;
}

/**
 * Get value in POST by key
 */
function getPost($key)
{
  return isset($_POST[$key]) ? $_POST[$key] : '';
}

// Err message
$errId;
$errPass;

// if (isset($data['err'])) {
//   $errs[] = $data['err'];
// }

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  if (empty(inputHandling($_POST["user"]))) {
    $errId = "Hãy nhập LoginID";
  } elseif (strlen($_POST['user']) < 4) {
    $errId = "Hãy nhập LoginID tối thiểu 4 ký tự";
  }

  if (empty(inputHandling($_POST["pass"]))) {
    $errPass = "Hãy nhập Password";
  } elseif (strlen($_POST['user']) < 4) {
    $errPass = "Hãy nhập Password tối thiểu 6 ký tự";
  }

  // Consider valid if $err is empty
  if (empty($errPass || $errId)) {
    header(
      'Location: http://localhost/gr09_library-management/lib-management/Login/authenticate/'
        . $_POST["user"] . '/'
        . $_POST["pass"]
    );
  }
}
?>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    body {
      margin: 0px;
    }

    .title-page {
      width: 100vw;
      height: 20vh;
    }

    .text-title_page {
      padding: 60px;
      line-height: 30px;
      font-size: 30px;
      text-align: center;
    }

    .body {
      width: 100vw;
      height: 80vh;
    }

    .main {
      border-radius: 5px;
      padding: 15px;
      width: 800px;
      height: 500px;
      background-color: #EBEBEB;
      margin-left: 400px;
    }

    .margin-top20 {
      margin-top: 20px;
    }

    * {
      box-sizing: border-box;
    }

    input[type=text],
    select,
    textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }

    label {
      padding: 12px 12px 12px 0;
      display: inline-block;
    }

    input[type=submit] {
      background-color: #04AA6D;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }

    input[type=submit]:hover {
      background-color: #45a049;
    }

    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }

    .col-25 {
      font-size: 18px;
      float: left;
      width: 25%;
    }

    .col-75 {
      float: left;
      width: 75%;
    }

    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {

      .col-25,
      .col-75,
      input[type=submit] {
        width: 100%;
        margin-top: 0;
      }
    }
  </style>
</head>

<body class="body">

  <div class="title-page">
    <div class="text-title_page" style="font-size: 60px; font-weight: bold;">Website quản lý thư viện</div>
  </div>
  <form action="" method="post" style="
                    width: 930px;
                    margin: 100px 470px;
                    padding-top: 50px;
                    padding-bottom: 50px;
                    background: #EBEBEB;">

    <div style="margin-top:20px; display: flex; justify-content: flex-end; margin-right: 40px">
      <label for="exampleInputEmail1" class="form-label" style="
                        width: 246px;
                        height: 42px;

                        font-family: 'Inter';
                        font-style: normal;
                        font-weight: 400;
                        font-size: 35px;
                        line-height: 42px;
                        text-align: center;

                        color: #000000;">Tên đăng nhập</label>
      <input type="text" class="form-control" name="user" placeholder="Tên đăng nhập" id="user" style="box-sizing: border-box;

                        margin-left: 20px;
                        width: 589px;
                        height: 58px;

                        background: #FFFFFF;
                        border: 1px solid #000000;" value="<?php echo getPost('user') ?>">
    </div>
    <div style="margin-bottom: 10px; color: red; margin-left: 300px; margin-top: 10px; font-size: 25px">
                          <?php
                          // Iterate through each error
                            if (!empty($errId)) {
                              echo "<div>$errId</div>";
                            }
                          ?>
                        </div>
    <div style="margin-top:25px; display: flex; justify-content: flex-end; margin-right: 40px">
      <label for="exampleInputPassword1" class="form-label" style="
                width: 154px;
                height: 42px;

                font-family: 'Inter';
                font-style: normal;
                font-weight: 400;
                font-size: 35px;
                line-height: 42px;
                text-align: center;

                color: #000000;">Mật khẩu</label>
      <input type="password" class="form-control" name="pass" id="password" style="
                box-sizing: border-box;
                width: 589px;
                height: 58px;
                margin-left: 96px;
                background: #FFFFFF;
                border: 1px solid #000000; padding: 12px; border-radius: 4px;" placeholder="Nhập mật khẩu" value="<?php echo getPost('pass') ?>">
    </div>
    <div style="margin-bottom: 10px; color: red; margin-left: 300px; margin-top: 10px; font-size: 25px">
                          <?php
                          // Iterate through each error
                            if (!empty($errPass)) {
                              echo "<div>$errPass</div>";
                            }
                          ?>
                        </div>
    <button type="submit" style="width: 156px;
                height: 46px;
                border: 2px solid gray;
                border-radius: 10px;
                margin-top: 50px;
                font-family: 'Inter';
                font-style: normal;
                font-weight: 400;
                font-size: 25px;
                box-shadow: 0px 5px gray;
                margin: 18px 390px;" class="btn btn-primary">Đăng nhập</button>

    <div style="text-align: center;">
      <a href="http://localhost/gr09_library-management/lib-management/Admin/forgotPassword" style="
    height: 36px;
    font-size: 22px;
    color: #0057FF;
    ">Quên mật khẩu?</a>
    </div>

  </form>

</body>

</html>