<?php
?>
<html>

<head>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            padding: 0;
        }

        .content {
            width: 400px;
            height: 100px;
            margin: 0 auto;
            margin-top: 100px;
            background-color: #EBEBEB;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
    </style>
</head>

<body>
    <div class="content">
        <Span>Reset password thành công!</Span>
        <a href="http://localhost/gr09_library-management/lib-management/Admin/resetPassword">Quay lại màn hình chính</a>
    </div>
</body>

</html>