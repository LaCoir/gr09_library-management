<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 10:51:58
  @ Description: Working hard improves my quality of life ^^
 -->

 <?php
header("Content-type: text/html; charset=utf-8");
class ConnectionDB{

    public $con;
    protected $servername = "52.53.203.35:3306";
    protected $username = "root";
    protected $password = "minhdang";
    protected $dbname = "gr09_dev";

    function __construct(){
        $this->con = mysqli_connect($this->servername, $this->username, $this->password);
        mysqli_set_charset($this->con, 'UTF8');
        if(!$this->con){
             die('Không tìm thấy kết nốt' . mysqli_connect_errno());
        }else {
            mysqli_select_db($this->con, $this->dbname);
        }
    }

}
?>
