<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-29 21:17:10
  @ Description: Working hard improves my quality of life ^^
 -->

 <?php
class App{
    #http://localhost/gr09_library-management/lib-management/Home/Default/1/2/3
    protected $controller="Home"; # =>Home
    protected $action="Default"; # =>Default
    protected $params=[]; # => 1/2/3
    
    function __construct(){
    # [0] => Home [1] => Default [2] => 1 [3] => 2 [4] => 3

        // handler controller(class)
        $arr_url = $this->UrlProcess();
  
        if(isset($arr_url[0])){
            if (file_exists("./mvc/controllers/".$arr_url[0]."Controller.php")) {
                $this -> controller = $arr_url[0];
            }
            require_once ('./mvc/controllers/' . $this->controller . 'Controller.php');
            
        }

        $this->controller = $this->controller . "Controller";

        // handler action(method)
        if(isset($arr_url[1])){
            if (method_exists($this->controller, $arr_url[1])) {
                $this->action = $arr_url[1];
            }
        }

        // handler params
        $this->params = array_slice($arr_url,2,sizeof($arr_url));

        $this->controller = new $this->controller; // controller = Home <String>
        call_user_func_array([$this->controller, $this->action], $this->params);
    }

    function UrlProcess(){
        if (isset($_GET["url"])) {
            $url = filter_var(trim($_GET["url"],"/"));
            return explode("/",$url);            
        }
    }
}
?>
