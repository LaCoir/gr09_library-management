<!--
  @ Author: Minh Dang
  @ Create Time: 2022-12-30 10:57:59
  @ Description: Working hard improves my quality of life ^^
 -->

 <?php
class BaseController{
    public function model($model){
        if (file_exists("./mvc/models/".$model.".php")) {
            require_once("./mvc/models/".$model.".php");
            return new $model;
        }
    }

    public function view($model,$data=[]){
        $view_name = $model . "View";
        if (file_exists("./mvc/views/".$view_name.".php")) {
            require_once "./mvc/views/".$view_name.".php";
        }
    }

    public function views($model,$data=[],$data2=[], $data3=[]){
        $view_name = $model . "View";
        if (file_exists("./mvc/views/".$view_name.".php")) {
            require_once "./mvc/views/".$view_name.".php";
        }
    }
}
?>